const buildServer = require("./server");
const PORT = 8080;

const server = buildServer();

const start = async () => {
  try {
    await server.listen(PORT);
    console.log(`listen to port ${PORT}`);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

start();
