const {
  getItemsOpts,
  getItemOpts,
  postItemOpts,
  deleteItemOpts,
  updateItemOpts,
} = require("./itemHandler");

function itemsRoutes(fastify, _, done) {
  //GET ALL ITEMS
  fastify.get("/items", getItemsOpts);

  //GET SINGLE ITEMS
  fastify.get("/items/:id", getItemOpts);

  //add item
  fastify.post("/items", postItemOpts);

  //delete item
  fastify.delete("/items/:id", deleteItemOpts);

  //update item
  fastify.put("/items/:id", updateItemOpts);

  done();
}

module.exports = itemsRoutes;
