let items = require("../../items");
const { v4: uuidv4 } = require("uuid");

const getItems = (request, response) => {
  response.send(items);
};

const getItem = (request, response) => {
  const { id } = request.params;

  const item = items.find((item) => {
    return item.id === id;
  });
  response.send(item);
};

const addItem = (request, response) => {
  const { name } = request.body;

  const item = {
    id: uuidv4(),
    name,
  };

  items = [...items, item];

  response.code(201).send(item);
};

const deleteItem = (request, response) => {
  const { id } = request.body;

  items = items.filter((item) => item.id !== id);

  response.send({ message: `Item ${id} has been remove` });
};

const updateItem = (request, response) => {
  const { id } = request.params;
  const { name } = request.body;

  items = items.map((item) => (item.id === id ? { id, name } : item));

  let item = items.find((item) => item.id === id);

  response.send(item);
};

module.exports = { getItems, getItem, addItem, deleteItem, updateItem };
