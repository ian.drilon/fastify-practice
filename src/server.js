const Fastify = require("fastify")({ logger: true });
const items = require("./routes/items");
const swagger = require("@fastify/swagger");

const buildServer = () => {
  const server = Fastify;

  const swaggerOption = {
    exposeRoute: true,
    routePrefix: "/docs",
    swagger: {
      info: {
        title: "fastify-api",
        description: "testing the fastify swagger api",
        version: "0.1.0",
      },
    },
  };

  server.register(swagger, swaggerOption);
  server.register(items);

  return server;
};

module.exports = buildServer;
