const buildServer = require("../src/server");
const { test } = require("tap");

test("Testing `/items` api if it return all items", async (t) => {
  const fastify = buildServer();

  const array = [
    { id: "1", name: "Item one" },
    { id: "2", name: "Item two" },
    { id: "3", name: "Item three" },
  ];

  t.teardown(() => {
    fastify.close();
  });

  const response = await fastify.inject({
    method: "GET",
    url: "/items",
  });

  t.equal(response.statusCode, 200);
  t.same(response.json(), array);

  t.end();
});
