const faker = require("@faker-js/faker");
const buildServer = require("../src/server");
const { test } = require("tap");

test("Testing POST `/items` api if add new item", async (t) => {
  const fastify = buildServer();
  const name = faker.name?.findName();

  t.teardown(() => {
    fastify.close();
  });

  const response = await fastify.inject({
    method: "POST",
    url: "/items",
    payload: {
      name,
    },
  });

  t.equal(response.statusCode, 201);

  const json = response.json();

  t.equal(json.name, name);
  console.log(json);

  t.end();
});

